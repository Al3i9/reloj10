const tiempo = document.querySelector('.tiempo');
fecha = document.querySelector('.fecha');

function relojDigital(){
    let hora = new Date(),
    dia = hora.getDate(),
    mes = hora.getMonth(),
    anio = hora.getFullYear(),
    diaSemana = hora.getDay();
    
    dia = ('0' + dia).slice(-2);

    let timeString = hora.toLocaleTimeString();
    tiempo.innerHTML = timeString;

    let semana = ['Domingo','Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
    let showSemana = (semana[diaSemana]);

    let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo','Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    let showMeses = (meses[mes]);

    fecha.innerHTML = `${showSemana}  ${ dia} de ${showMeses} del ${anio}`
}

setInterval(() => {
    relojDigital()
}, 1000);


